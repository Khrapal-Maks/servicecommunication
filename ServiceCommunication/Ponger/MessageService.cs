﻿using RabbitMQ.Wrapper;
using System;
using System.Threading;

namespace Ponger
{
    //First start? because listet pinger
    public class MessageService
    {
        private readonly string _ping_queue = "ping_queue";
        private readonly string _pong_queue = "pong_queue";
        private readonly string _message = "pong";
        private readonly int _timeSleepThread = 2500;

        private readonly IMessageQueueService _messageQueueService;

        public MessageService()
        {
            _messageQueueService = new MessageQueueService();
            Listen();
        }

        public void Send()
        {
            _messageQueueService.SendMessageToQueue(_ping_queue, _message);
            Thread.Sleep(_timeSleepThread);
            Listen();
        }

        public void Listen()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            _messageQueueService.ListenQueue(_pong_queue);
            Thread.Sleep(_timeSleepThread);
            Send();
        }    
    }
}
