﻿using RabbitMQ.Wrapper;
using System;
using System.Threading;

namespace Pinger
{
    public class MessageService
    {
        private readonly string _ping_queue = "ping_queue";
        private readonly string _pong_queue = "pong_queue";
        private readonly string _message = "ping";
        private readonly int _timeSleepThread = 2500;

        private readonly IMessageQueueService _messageQueueService;

        public MessageService()
        {
            _messageQueueService = new MessageQueueService();
            Send();
        }

        public void Send()
        {
            _messageQueueService.SendMessageToQueue(_pong_queue, _message);
            Thread.Sleep(_timeSleepThread);
            Listen();
        }

        public void Listen()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            _messageQueueService.ListenQueue(_ping_queue);
            Thread.Sleep(_timeSleepThread);
            Send();
        }
    }
}
