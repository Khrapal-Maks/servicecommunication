﻿namespace RabbitMQ.Wrapper
{
    public interface IMessageQueueService
    {
        void ListenQueue(string queue);
        void SendMessageToQueue(string queue, string message);
    }
}
