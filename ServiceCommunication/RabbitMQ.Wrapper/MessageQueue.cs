﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class MessageQueueService : IMessageQueueService
    {
        private IConnectionFactory _factory;
        private IConnection _connection;
        private IModel _channel;
        private EventingBasicConsumer _consumer;

        public MessageQueueService()
        {
            _factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
        }

        public void ListenQueue(string queue)
        {
            _channel.QueueDeclare(queue: queue,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            _consumer = new EventingBasicConsumer(_channel);

            _consumer.Received += (sender, e) =>
            {
                var message = Encoding.UTF8.GetString(e.Body.ToArray());
                Console.WriteLine($"{DateTime.Now} {message}");
                _channel.BasicAck(e.DeliveryTag, false);
            };

            _channel.BasicConsume(queue: queue,
                autoAck: false,
                consumer: _consumer);
        }

        public void SendMessageToQueue(string queue, string message)
        {
            _channel.QueueDeclare(queue: queue,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var body = Encoding.UTF8.GetBytes(message);

            var properties = _channel.CreateBasicProperties();
            properties.Persistent = true;

            _channel.BasicPublish(exchange: "",
                                 routingKey: queue,
                                 basicProperties: properties,
                                 body: body);
        }
    }
}
